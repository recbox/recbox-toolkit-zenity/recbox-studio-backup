#!/usr/bin/env bash

SYS_LANG=$(echo "$LANG" | cut -d '.' -f 1)
if [[ -z $(find /usr/share/recbox-studio-backup/translations/settings/ -name "$SYS_LANG".trans) ]]; then
    source /usr/share/recbox-studio-backup/translations/settings/en_US.trans
else
    for TRANS in /usr/share/recbox-studio-backup/translations/settings/"$SYS_LANG"*; do
        source $TRANS
    done
fi
